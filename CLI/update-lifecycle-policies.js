require('dotenv').config();
const { Client } = require('@elastic/elasticsearch');

const client = new Client({
  node: 'http://18.200.137.135:9200',
  auth: {
    username: process.env.KIBANA_USERNAME,
    password: process.env.KIBANA_PASSWORD,
  }
});

async function run() {
  await client.ilm.putLifecycle({
    policy: '2day-retention',
    body: {
      "policy": {
        "phases": {
          "delete": {
            "min_age": "2d",
            "actions": {
              "delete": {}
            }
          }
        }
      }
    }
  });
  await client.ilm.putLifecycle({
    policy: '2week-retention',
    body: {
      "policy": {
        "phases": {
          "delete": {
            "min_age": "14d",
            "actions": {
              "delete": {}
            }
          }
        }
      }
    }
  });
  await client.ilm.putLifecycle({
    policy: '4week-retention',
    body: {
      "policy": {
        "phases": {
          "delete": {
            "min_age": "28d",
            "actions": {
              "delete": {}
            }
          }
        }
      }
    }
  });
  await client.ilm.putLifecycle({
    policy: '2months-retention',
    body: {
      "policy": {
        "phases": {
          "delete": {
            "min_age": "60d",
            "actions": {
              "delete": {}
            }
          }
        }
      }
    }
  });
  await client.ilm.putLifecycle({
    policy: '2000years-retention',
    body: {
      "policy": {
        "phases": {
          "delete": {
            "min_age": "730000d",
            "actions": {
              "delete": {}
            }
          }
        }
      }
    }
  });
}

run();

require('dotenv').config();
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
const {Client} = require('@elastic/elasticsearch');
const fs = require('fs');
const path = require('path');

const client = new Client({
    node: 'http://18.200.137.135:9200',
    auth: {
        username: process.env.KIBANA_USERNAME,
        password: process.env.KIBANA_PASSWORD,
    }
});

async function getInput(text) {
    return new Promise((resolve) => {
        readline.question(text, (input) => {
            resolve(input);
        })
    });
}

async function run() {
    console.log('There are number of steps involved. Please follow carefully.');
    let system = (await getInput(`Enter the system name you want to update: `)).trim();

    let envs = (await getInput('Enter the comma separated list of env you want to update: '))
        .trim().split(',').map((x) => x.trim());

    console.log('Entered system:', system);
    console.log('Entered envs:', envs);
    await getInput('Press enter to continue: ');
    return ;

    console.log('Step 1: Modify _template.json and run `node ./update-templates.js` to update them in ES');
    let loopAgain = true;
    while (loopAgain) {
        await new Promise((resolve) => {
            readline.question(`Enter "continue" to proceed or anything else to cancel: `, (input) => {
                if (input === 'continue') {
                    loopAgain = false;
                    readline.close();
                } else {
                    console.log('Exiting');
                    process.exit(0);
                }
                resolve();
            })
        });
    }

    console.log('Step 2: Update logstash configuration to start sending the data to the swap indexes');
    loopAgain = true;
    while (loopAgain) {
        await new Promise((resolve) => {
            readline.question(`Enter "continue" to proceed or anything else to cancel: `, (input) => {
                if (input === 'continue') {
                    loopAgain = false;
                    readline.close();
                } else {
                    console.log('Exiting');
                    process.exit(0);
                }
                resolve();
            })
        });
    }
}

run();

require('dotenv').config();
const {Client} = require('@elastic/elasticsearch');
const fs = require('fs');
const path = require('path');

console.log(`Initiating kibana access using username: ${process.env.KIBANA_USERNAME} and password: ${process.env.KIBANA_PASSWORD.substring(0,3)+ "..."}`)
const client = new Client({
    node: 'http://18.200.137.135:9200',
    auth: {
        username: process.env.KIBANA_USERNAME,
        password: process.env.KIBANA_PASSWORD,
    }
});

//const baseTemplate = fs.readFileSync(path.join('./', '_template.json')).toString();
//const boTemplate = fs.readFileSync(path.join('./', '_bo-template.json')).toString();
const mappingsTemplate = JSON.parse(fs.readFileSync(path.join('./', '_mapping_template.json')).toString());


const retention2days = '2day-retention';
const retention2weeks = '2week-retention';
const retention4weeks = '4week-retention';
const retention2months = '2months-retention';
const retention1year = '1year-retention';
const retentionForever = '2000years-retention';

function getTemplate(name, indexPatterns, order, numberOfShards, numberOfReplicas, retention, mappings) {
    return {        
        "name": name,
        "index_patterns": indexPatterns,
        "order": order,
        "settings": {
            "number_of_shards": numberOfShards,
            "number_of_replicas": numberOfReplicas,
            "index.lifecycle.name": retention,
            "index.mapping.total_fields.limit": 1000
        },
        "mappings": mappings
    };
};

const localEnvs = (suffix => ["a", "b", "c"].map(env => env + suffix));
const nonpEnvs =  (suffix => ["d", "e", "f", "r", "s", "t"].map(env => env + suffix));
const prodEnvs =  (suffix => ["q", "p", "o"].map(env => env + suffix));


const templateData = [
    // nonproduction
    getTemplate("template-nonp-default-logging",nonpEnvs("-*"), 1,  1, 0, retention4weeks, mappingsTemplate),
    getTemplate("template-nonp-businessobjects",nonpEnvs("-*-bo-*"), 2, 1, 0, retentionForever, mappingsTemplate),
    getTemplate("template-nonp-errors", nonpEnvs("-*-error*"), 2, 1, 0, retention1year, mappingsTemplate),
    getTemplate("template-nonp-events", nonpEnvs("-*-event*"), 2, 1, 0, retention1year, mappingsTemplate),

    // production
    getTemplate("template-prod-default-logging",prodEnvs("-*"), 1,  1, 0, retention4weeks, mappingsTemplate),
    getTemplate("template-prod-outboundhttpcall",prodEnvs("-*-outboundhttpcall*"), 2,  1, 0, retention4weeks, mappingsTemplate),
    getTemplate("template-prod-inboundhttpcall",prodEnvs("-*-inboundhttpcall*"), 2,  1, 0, retention4weeks, mappingsTemplate),
    getTemplate("template-prod-businessobjects",prodEnvs("-*-bo-*"), 2, 1, 1, retentionForever, mappingsTemplate),
    getTemplate("template-prod-events",prodEnvs("-*-event*"), 2, 1, 1, retentionForever, mappingsTemplate),
    getTemplate("template-prod-errors",prodEnvs("-*-error*"), 2, 1, 0, retention1year, mappingsTemplate),

    // localhost
    getTemplate("template-localhost-default-logging", localEnvs("-*"), 1, 1, 0, retention2days, mappingsTemplate),
    getTemplate("template-dsgapps", ["dsgapps*"], 1, 2, 1, retention1year, {}),


];



async function run() {
    await Promise.all(templateData.map((temp) => {
        const name = temp.name;
        delete temp.name;

        return client.indices.putTemplate({
            name: name,
            body: temp
        }).then(() => {
            console.log("Posted template " + name);
        }).catch(err => {
            console.log("Failed template " + name);
            console.log(err);
        });
    }));
}

run();


/*const templateData = [];
[
    {system: 'mimir', retention: {a: r1, d: r2, t: r2, q: r2, p: r3}},
    {system: 'heimdalbackend', retention: {a: r1, d: r2, t: r2, q: r2, p: r3}},
    {system: 'bifrostbackend', retention: {a: r1, d: r2, d2: r2, r: r2, t: r2, t2: r2, o: r2, p: r3}},
].forEach(project => {
    for (const r in project.retention) {
        templateData.push({
            template: baseTemplate,
            name: `${r}-${project.system}`,
            pattern: `${r}-${project.system}*`,
            retention: project.retention[r],
            order: 1,
        });
        templateData.push({
            template: baseTemplate,
            name: `${r}-${project.system}-debug`,
            pattern: `${r}-${project.system}-debug*`,
            retention: r0,
            order: 10,
        });
        templateData.push({
            template: boTemplate,
            name: `${r}-${project.system}-bo`,
            pattern: `${r}-${project.system}-bo-*`,
            retention: project.retention[r],
            order: 50,
        });
    }
});

async function run() {
    await Promise.all(templateData.map(async(obj) => {
        const temp = obj.template
            .replace('{{INDEX_PATTERN}}', obj.pattern)
            .replace('{{RETENTION}}', obj.retention)
            .replace('{{ORDER}}', obj.order);

        await client.indices.putTemplate({
            name: obj.name + '-template',
            body: JSON.parse(temp),
        });
        return obj.name + '-template';
    })).then((template) => {
        console.log("Created template: " + template);
    }).catch(err => {
        console.log(err);
    });
}

run();
*/

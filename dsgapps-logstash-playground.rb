class Event
  @data
  def initialize(data)
      @data = data
  end

  def get(key)
    result = @data[key.to_sym]
    if result.is_a?(Hash)
      result = result.transform_keys(&:to_s)
    end
    result
  end
end
baseHash = {
  system: 'HeimdalBackend',
  component: 'HeimdalBackendApi',
  env: 'd',
  systemEnv: 'd-HeimdalBackend',
  logSource: 'test',
  client: {
    clientVersion: '1.0.5',
    platform: 'iOS',
    platformVersion: '10.10',
    platformAndClientVersion: 'iOS 1.0.5',
    traceId: 'CT-12345678',
  },
  product: {
    name: 'Scan and Go',
    operationName: 'unknown',
    operationTraceId: 'OT-1234567890'
  },
}

deprecationAlertHash = baseHash.merge({
  logType: 'DeprecationAlert',
  message: 'Feature XXX has been deprecated',
  deprecatedSince: '1.0.5',
})

eventFailHash = baseHash.merge({
  logType: 'Event',
  message: 'Event Checkout',
  event: {
    name: 'Checkout',
    category: 'Basket',
    data: 'zzz'
  },
})

eventHash = baseHash.merge({
  logType: 'Event',
  message: 'Event Checkout',
  event: {
    name: 'Checkout',
    category: 'Basket',
    checkoutData: 'zzz'
  },
})

crudHash = baseHash.merge({
  logType: 'Crud',
  message: 'Crud Upsert for Basket',
  crud: {
    operation: 'Upserted',
    entity: 'Basket',
    name: 'BasketUpserted',
    basketUpsertedData: {adsf:'asdf'}
  }
})

httpInboundHash = baseHash.merge({
  logType: 'InboundHttpCall',
  message: 'Request start',
  http: {
    url: 'https://api.com/event/show/13',
    path: '/event/show/13',
    method: 'get',
    route: '/event/show/{id}',
    requestTraceId: 'RT-12345678',
  }
})

event = Event.new(httpInboundHash);
messages = []

serverInitiated = event.get("serverInitiated")
logType = event.get("logType")
product = event.get("product")

  # All requests should have these
if logType.nil?
  messages << "logType is required"
elsif !['DeprecationAlert', 'Event', 'TextLog', 'Error', 'Crud',  'InboundHttpCall', 'OutboundHttpCall', 'Kafka'].include?(logType)
  messages << "logType has invalid value"
end

messages << "system is required" if event.get("system").nil?
messages << "component is required" if event.get("component").nil?
messages << "env is required" if event.get("env").nil?
messages << "systemEnv is required" if event.get("systemEnv").nil?
messages << "logSource is required" if event.get("logSource").nil?
messages << "message is required" if event.get("message").nil?

# Product checks
if product.nil?
    messages << "product is required" if product.nil?
else
    messages << "product.name is required" if product && product['name'].nil?
    messages << "product.operationName is required" if product['operationName'].nil?
    messages << "product.operationTraceId is required" if product['operationTraceId'].nil?
    messages << "product.operationTraceId should start with OT" unless !product['operationTraceId'].nil? && product['operationTraceId'].start_with?("OT-")
end

unless serverInitiated
  # Client checks
  client = event.get("client")
  if client.nil?
    messages << "client is required"
  else
    messages << "client.clientVersion is required" if client['clientVersion'].nil?
    messages << "client.platform is required" if client['platform'].nil?
    messages << "client.platformVersion is required" if client['platformVersion'].nil?
    messages << "client.platformAndClientVersion is required" if client['platformAndClientVersion'].nil?
    messages << "client.traceId is required" if client['traceId'].nil?
    messages << "client.traceId should start with CT" unless !client['traceId'].nil? && client['traceId'].start_with?("CT-")
  end
end

if logType === "DeprecationAlert"
  if event.get('deprecatedSince').nil?
    messages << "deprecatedSince is required"
  end
elsif logType === "Event"
  ev = event.get("event")
  if ev.nil?
    messages << "event is required"
  else
    messages << "event.name is required" if ev['name'].nil?
    messages << "event.category is required" if ev['category'].nil?
    camelisedName = ev['name'][0].downcase  + ev['name'][1..-1] +'Data'
    ev.keys.each do |key|
      if !['name', 'category', 'domain', camelisedName].include?(key)
      messages << "event." + key + " is not allowed. Did you mean " + camelisedName
      end
    end
  end
elsif logType === "Crud"
  crud = event.get("crud")
  if crud.nil?
    messages << "crud is required"
  else
    crudOperation = crud['operation']
    if crudOperation.nil?
      messages << "crud.operation is required" if crud['name'].nil?
    else
      messages << "crud.operation " + crudOperation + " is not allowed. Valid vaules are Upserted and Deleted" if !["Upserted", "Deleted"].include?(crudOperation)
    end
    messages << "crud.entity is required" if crud['entity'].nil?

    crudName = crud['name']
    if crudName.nil?
      messages << "crud.name is required"
    else
      camelisedName = crudName[0].downcase  + crudName[1..-1] +'Data'
      crud.keys.each do |key|
        if !['name', 'operation', 'entity', camelisedName].include?(key)
        messages << "crud." + key + " is not allowed. Did you mean " + camelisedName
        end
      end
    end
  end
elsif logType === "InboundHttpCall"
  http = event.get("http")
  if http.nil?
    messages << "http is required"
  else
    messages << "http.url is required" if http['url'].nil?
    messages << "http.path is required" if http['path'].nil?
    messages << "http.method is required" if http['method'].nil?
    messages << "http.route is required" if http['route'].nil?
    requestTraceId = http['requestTraceId']
    if requestTraceId.nil?
      messages << "http.requestTraceId is required"
    else
      messages << "http.requestTraceId should start with RT" if !requestTraceId.start_with?("RT-")
    end
  end
elsif logType === "OutboundHttpCall"
  httpOut = event.get("httpOutbound")
  if httpOut.nil?
    messages << "httpOutbound is required"
  else
    messages << "httpOutbound.url is required" if http['url'].nil?
    messages << "httpOutbound.path is required" if http['path'].nil?
    messages << "httpOutbound.method is required" if http['method'].nil?
    messages << "httpOutbound.route is required" if http['route'].nil?
    # messages << "httpOutbound.queryString is required" if http['queryString'].nil?
    # messages << "httpOutbound.payload is required" if http['payload'].nil?
    messages << "httpOutbound.headers is required" if http['headers'].nil?
    messages << "httpOutbound.responseTime is required" if http['responseTime'].nil?
    messages << "httpOutbound.success is required" if http['success'].nil?
    messages << "httpOutbound.statusCode is required" if http['statusCode'].nil?
    messages << "httpOutbound.responsePayload is required" if http['responsePayload'].nil?
    messages << "httpOutbound.responseHeaders is required" if http['responseHeaders'].nil?
  end
elsif logType === "Error"
  error = event.get("error")
  if error.nil?
    messages << "error is required"
  else
    # messages << "error.code is required" if error['code'].nil?
    errorTraceId = error['traceId']
    if errorTraceId.nil?
      messages << "error.traceId is required"
    else
      messages << "error.traceId should start with ERR" if !errorTraceId.start_with?("ERR-")
    end
    # messages << "error.innerError is required" if error['innerError'].nil?
    # messages << "error.stackTrace is required" if error['stackTrace'].nil?

    errorName = error['name']
    if errorName.nil?
      messages << "error.name is required"
    else
      camelisedName = errorName[0].downcase  + errorName[1..-1] +'Data'
      error.keys.each do |key|
        if !['name', 'code', 'traceId', 'innerError', 'stackTrace', camelisedName].include?(key)
          messages << "error." + key + " is not allowed. Did you mean " + camelisedName
        end
      end
    end
  end
elsif logType === "Kafka"
  kafka = event.get("kafka")
  if kafka.nil?
      messages << "kafka is required"
  else
      messages << "kafka.partition is required" if kafka['partition'].nil?
      messages << "kafka.offset is required" if kafka['offset'].nil?
      topicName = kafka['topic']
      if topicName.nil?
        messages << "kafka.topic is required"
      else
        camelisedName = topicName[0].downcase  + topicName[1..-1] +'Message'
        kafka.keys.each do |key|
          if !['topic', 'partition', 'offset', camelisedName].include?(key)
            messages << "kafka." + key + " is not allowed. Did you mean " + camelisedName
          end
        end
      end
  end
end

if messages.length > 0
  message = event.get("message")
  message = '' if message.nil?
  puts messages.join("\n") + "\n" + message
end

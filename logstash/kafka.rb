# the value of `params` is the value of the hash passed to `script_params`
# in the logstash configuration
def register(params)
# @drop_percentage = params["percentage"]
end

# the filter method receives an event and must return a list of events.
# Dropping an event means not including it in the return array,
# while creating new ones only requires you to add a new instance of
# LogStash::Event to the returned array
def filter(event)
    messages = []

    server_initiated = event.get("serverInitiated")
    log_type = event.get("logType")
    product = event.get("product")

    # All requests should have these
    if log_type.nil?
      messages << "logType is required"
    elsif !['Kafka'].include?(log_type)
      messages << "logType has invalid value"
    end
    messages << "system is required" if event.get("system").nil?
    messages << "component is required" if event.get("component").nil?
    messages << "env is required" if event.get("env").nil?
    messages << "systemEnv is required" if event.get("systemEnv").nil?
    messages << "logSource is required" if event.get("logSource").nil?
    messages << "message is required" if event.get("message").nil?

    # Product checks
    if product.nil?
        messages << "product is required" if product.nil?
    else
        messages << "product.name is required" if product && product['name'].nil?
        messages << "product.operationName is required" if !server_initiated && product['operationName'].nil?
        messages << "product.operationTraceId is required" if !server_initiated && product['operationTraceId'].nil?
        messages << "product.operationTraceId should start with OT" unless server_initiated || (!product['operationTraceId'].nil? && product['operationTraceId'].start_with?("OT-"))
    end

    unless server_initiated
      # Client checks
      client = event.get("client")
      if client.nil?
        messages << "client is required"
      else
        messages << "client.clientVersion is required" if client['clientVersion'].nil?
        messages << "client.platform is required" if client['platform'].nil?
        messages << "client.platformVersion is required" if client['platformVersion'].nil?
        messages << "client.platformAndClientVersion is required" if client['platformAndClientVersion'].nil?
        messages << "client.traceId is required" if client['traceId'].nil?
        messages << "client.traceId should start with CT" unless !client['traceId'].nil? && client['traceId'].start_with?("CT-")
      end
    end

    if log_type === "Kafka"
        kafka = event.get("kafka")
        if kafka.nil?
          messages << "kafka is required"
        else
          messages << "kafka.producer is required" if kafka['producer'].nil?
          messages << "kafka.partition is required" if kafka['partition'].nil?
          messages << "kafka.offset is required" if kafka['offset'].nil?
          topic_name = kafka['topic']
          if topic_name.nil?
            messages << "kafka.topic is required"
          else
            project = topic_name[0, topic_name.index('-')];
            command_name = topic_name[topic_name.index('-', project.length + 3) + 1 .. -1]
            camelised_name = project + command_name.gsub('-', '') + 'Message'
            kafka.keys.each do |key|
              if !['producer', 'topic', 'partition', 'offset', camelised_name].include?(key)
                messages << "kafka." + key + " is not allowed. Did you mean " + camelised_name
              end
            end
          end
        end
    end

    if messages.length > 0
        event.set("message", messages.join("\n") + "\n" + event.get("message"))
        event.set("schemaValidationError", true)
    end

    return [event]
end

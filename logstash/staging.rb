# the value of `params` is the value of the hash passed to `script_params`
# in the logstash configuration
def register(params)
# @drop_percentage = params["percentage"]
end

# the filter method receives an event and must return a list of events.
# Dropping an event means not including it in the return array,
# while creating new ones only requires you to add a new instance of
# LogStash::Event to the returned array
def filter(event)
    messages = []

    server_initiated = event.get("serverInitiated")
    log_type = event.get("logType")
    product = event.get("product")
    system = event.get("system")
    if !system.nil?
        system = system.downcase
        event.set("system", system)
    end
    event.set("logTypeLowercase", event.get("logType").downcase) if event.get("logType")

    force_prod = ["heimdalbackend", "mimir"]
    if force_prod.include?(system)
        event.set("type", "prod")
    end

    # HACK FOR HEIMDAL
    if system === "heimdalbackend"
        amount = event.get("amount")
        if !amount.nil?
            event.get("[http][customData][amount]", amount)
            event.remove("amount")
        end

        # Disable /api/common/log
        http = event.get("http")
        if !http.nil? && http['path'] === "/api/common/log"
            return []
        end
    end

    # All requests should have these
    if log_type.nil?
      messages << "logType is required"
    elsif !['DeprecationAlert', 'Event', 'TextLog', 'Error', 'Crud',  'InboundHttpCall', 'OutboundHttpCall', 'Debug'].include?(log_type)
      messages << "logType has invalid value"
    end
    messages << "system is required" if system.nil?
    messages << "component is required" if event.get("component").nil?
    messages << "env is required" if event.get("env").nil?
    messages << "systemEnv is required" if event.get("systemEnv").nil?
    messages << "logSource is required" if event.get("logSource").nil?

    # Nikola: We have agreed to no longer build schema validation into the logstash processing logic 
    return [event]
    messages << "message is required" if event.get("message").nil?

    # Product checks
    if product.nil?
        messages << "product is required" if product.nil?
    else
        messages << "product.name is required" if product && product['name'].nil?
        messages << "product.operationName is required" if !server_initiated && product['operationName'].nil?
        messages << "product.operationTraceId is required" if !server_initiated && product['operationTraceId'].nil?
        messages << "product.operationTraceId should start with OT" unless server_initiated || (!product['operationTraceId'].nil? && product['operationTraceId'].start_with?("OT-"))
    end

    unless server_initiated
      # Client checks
      client = event.get("client")
      if client.nil?
        messages << "client is required"
      else
        messages << "client.clientVersion is required" if client['clientVersion'].nil?
        messages << "client.platform is required" if client['platform'].nil?
        messages << "client.platformVersion is required" if client['platformVersion'].nil?
        messages << "client.platformAndClientVersion is required" if client['platformAndClientVersion'].nil?
        messages << "client.traceId is required" if client['traceId'].nil?
        messages << "client.traceId should start with CT" unless !client['traceId'].nil? && client['traceId'].start_with?("CT-")
      end
    end

    if log_type === "DeprecationAlert"
      if event.get('deprecatedSince').nil?
        messages << "deprecatedSince is required"
      end
    elsif log_type === "Event"
      ev = event.get("event")
      if ev.nil?
        messages << "event is required"
      else
        messages << "event.name is required" if ev['name'].nil?
        messages << "event.category is required" if ev['category'].nil?
        camelised_name = ev['name'][0].downcase + ev['name'][1..-1]  + 'Data'
        ev.keys.each do |key|
          if !['name', 'category', 'domain', camelised_name].include?(key)
          messages << "event." + key + " is not allowed. Did you mean " + camelised_name
          end
        end
      end
    elsif log_type === "Crud"
      crud = event.get("crud")
      if crud.nil?
        messages << "crud is required"
      else
        crud_operation = crud['operation']
        if crud_operation.nil?
          messages << "crud.operation is required" if crud['name'].nil?
        else
          messages << "crud.operation " + crud_operation + " is not allowed. Valid vaules are Upserted and Deleted" if !["Upserted", "Deleted"].include?(crud_operation)
        end
        messages << "crud.entity is required" if crud['entity'].nil?

        crud_name = crud['name']
        if crud_name.nil?
          messages << "crud.name is required"
        else
          camelised_name = crud_name[0].downcase + crud_name[1..-1] + 'Data'
          crud.keys.each do |key|
            if !['name', 'operation', 'entity', camelised_name].include?(key)
            messages << "crud." + key + " is not allowed. Did you mean " + camelised_name
            end
          end
        end
      end
    elsif log_type === "InboundHttpCall"
      http = event.get("http")
      if http.nil?
        messages << "http is required"
      else
        messages << "http.url is required" if http['url'].nil?
        messages << "http.path is required" if http['path'].nil?
        messages << "http.method is required" if http['method'].nil?
        messages << "http.route is required" if http['route'].nil?
        request_trace_id = http['requestTraceId']
        if request_trace_id.nil?
          messages << "http.requestTraceId is required"
        else
          messages << "http.requestTraceId should start with RT" if !request_trace_id.start_with?("RT-")
        end
      end
    elsif log_type === "OutboundHttpCall"
      http_out = event.get("httpOutbound")
      if http_out.nil?
        messages << "httpOutbound is required"
      else
        messages << "httpOutbound.url is required" if http_out['url'].nil?
        messages << "httpOutbound.path is required" if http_out['path'].nil?
        messages << "httpOutbound.method is required" if http_out['method'].nil?
        messages << "httpOutbound.route is required" if http_out['route'].nil?
        # messages << "httpOutbound.queryString is required" if http['queryString'].nil?
        # messages << "httpOutbound.payload is required" if http['payload'].nil?
        messages << "httpOutbound.headers is required" if http_out['headers'].nil?
        messages << "httpOutbound.responseTime is required" if http_out['responseTime'].nil?
        messages << "httpOutbound.success is required" if http_out['success'].nil?
        messages << "httpOutbound.statusCode is required" if http_out['statusCode'].nil?
        messages << "httpOutbound.responsePayload is required" if http_out['responsePayload'].nil?
        messages << "httpOutbound.responseHeaders is required" if http_out['responseHeaders'].nil?
      end
    elsif log_type === "Error"
      error = event.get("error")
      if error.nil?
        messages << "error is required"
      else
        # messages << "error.code is required" if error['code'].nil?
        error_trace_id = error['traceId']
        if error_trace_id.nil?
          messages << "error.traceId is required"
        else
          messages << "error.traceId should start with ERR" if !error_trace_id.start_with?("ERR-")
        end
        # messages << "error.innerError is required" if error['innerError'].nil?
        # messages << "error.stackTrace is required" if error['stackTrace'].nil?

        error_name = error['name']
        if error_name.nil?
          messages << "error.name is required"
        else
          camelised_name = error_name[0].downcase + error_name[1..-1] + 'Data'
          error.keys.each do |key|
            if !['name', 'code', 'traceId', 'innerError', 'stackTrace', camelised_name].include?(key)
              messages << "error." + key + " is not allowed. Did you mean " + camelised_name
            end
          end
        end
      end
    end

    if messages.length > 0
        event.set("message", messages.join("\n") + "\n" + event.get("message"))
        event.set("schemaValidationError", true)
    end

    return [event]
end
